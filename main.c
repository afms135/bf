#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

//Function signatures
void *mmap_file(char *path, size_t *size);
void unmap_file(void *ptr, size_t size);
char *mmap_err(void);
void mmap_errfree(char *str);

//POSIX
#if defined(unix) || defined(__unix__) || defined(__unix)
#include <fcntl.h>    //open()
#include <sys/stat.h> //fstat()
#include <sys/mman.h> //mmap()
#include <unistd.h>   //close()

void *mmap_file(char *path, size_t *size)
{
	int fd = open(path, O_RDONLY);
	if(fd < 0)
		goto OERR;

	struct stat s;
	int ret = fstat(fd, &s);
	if(ret < 0)
		goto ERR;

	void *ptr = mmap(NULL, s.st_size, PROT_READ, MAP_SHARED, fd, 0);
	if(ptr == MAP_FAILED)
		goto ERR;

	close(fd);
	*size = s.st_size;
	return ptr;

ERR:	close(fd);	
OERR:	return NULL;
}

void unmap_file(void *ptr, size_t size)
{
	munmap(ptr, size);
}

char *mmap_err(void)
{
	return strerror(errno);
}

void mmap_errfree(char *str)
{
	(void)str; //Not required
}

//Windows
#elif defined(_WIN32)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

void *mmap_file(char *path, size_t *size)
{
	HANDLE fh = CreateFile(
			path,
			GENERIC_READ,
			0,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			NULL);
	if(fh == INVALID_HANDLE_VALUE)
		goto OERR;

	DWORD siz_high;
	DWORD siz_low = GetFileSize(fh, &siz_high);
	if(siz_low == INVALID_FILE_SIZE)
		goto FERR;

	HANDLE mh = CreateFileMapping(
			fh,
			NULL,
			PAGE_READONLY,
			siz_high,
			siz_low,
			NULL);
	if(mh == NULL)
		goto FERR;

	void *ptr = MapViewOfFile(mh, FILE_MAP_READ, 0, 0, 0);
	if(ptr == NULL)
		goto MERR;

	*size = ((size_t)siz_high<<sizeof(DWORD)*8) | siz_low;
	CloseHandle(mh);
	CloseHandle(fh);
	return ptr;

MERR:	CloseHandle(mh);
FERR:	CloseHandle(fh);
OERR:	return NULL;
}

void unmap_file(void *ptr, size_t size)
{
	(void)size; //Not used in win32
	UnmapViewOfFile(ptr);
}

char *mmap_err(void)
{
	LPSTR str;
	DWORD ret = FormatMessage(
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_ALLOCATE_BUFFER,
			NULL,
			GetLastError(),
			LANG_SYSTEM_DEFAULT,
			(LPTSTR)&str,
			0,
			NULL);
	return (ret) ? str : NULL;
}

void mmap_errfree(char *str)
{
	LocalFree(str);
}
#else
	#error "Platform not supported"
#endif

int main(int argc, char *argv[])
{
	uint8_t *tape;
	uint16_t tapeptr = 0;
	char *prog;
	size_t size;
	size_t ip = 0;

	if(argc != 2)
	{
		fprintf(stderr, "Usage: %s <file.b>\n", argv[0]);
		return -1;
	}

	tape = calloc(1, 30000);
	if(tape == NULL)
	{
		fprintf(stderr, "Could not allocate memory: %s\n", strerror(errno));
		return -1;
	}

	prog = mmap_file(argv[1], &size);
	if(prog == NULL)
	{
		char *errstr = mmap_err();
		fprintf(stderr, "Could not mmap file (%s): %s\n", argv[1], errstr);
		mmap_errfree(errstr);
		return -1;
	}

	while(ip < size)
	{
		switch(prog[ip])
		{
		case '>':
			if(tapeptr == 30000-1)
			{
				fprintf(stderr, "Tape moved beyond 30000\n");
				goto ERR;
			}
			tapeptr++;
			break;
		case '<':
			if(tapeptr == 0)
			{
				fprintf(stderr, "Tape moved below 0\n");
				goto ERR;
			}	
			tapeptr--;	
			break;
		case '+':	
			tape[tapeptr]++;	
			break;
		case '-':	
			tape[tapeptr]--;
			break;
		case '.':	
			putchar(tape[tapeptr]);
			break;
		case ',':	
			tape[tapeptr] = getchar();
			break;
		case '[':
			if(tape[tapeptr] == 0)
			{
				int cnt = 0;
				while(1)
				{
					if(prog[ip] == '[')
						cnt++;
					else if(prog[ip] == ']')
						cnt--;
					if(cnt == 0)
						break;

					if(ip == size-1)
					{
						fprintf(stderr, "Mismatched brackets\n");
						goto ERR;
					}
					ip++;
				}
			}
			break;
		case ']':
			if(tape[tapeptr] != 0)
			{
				int cnt = 0;
				while (1)
				{
					if (prog[ip] == '[')
						cnt--;
					else if (prog[ip] == ']')
						cnt++;
					if (cnt == 0)
						break;

					if(ip == 0)
					{
						fprintf(stderr, "Mismatched brackets\n");
						goto ERR;
					}
					ip--;
				}
			}
			break;
		}
		ip++;
	}

	free(tape);
	unmap_file(prog, size);
	return 0;

ERR:	free(tape);
	unmap_file(prog, size);
	return -1;
}
